import React from 'react';

const MonBouton = (props) => {
    const {titre} = props
    return (
        <div>
            <button className='btn btn-primary m-3'> Je m appelle {titre} </button>
        </div>
    );
}

export default MonBouton;
