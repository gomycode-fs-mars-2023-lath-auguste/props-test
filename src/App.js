import React from 'react';
import MonBouton from './MonBouton';

const App = () => {
  return (
    <div>
      <h1>Welcome</h1>
      <div>
        <MonBouton titre="jean"/>
        <MonBouton titre="paul"/>
        <MonBouton titre="luc"/>

      </div>
    </div>
  );
}

export default App;
